package nl.utwente.di.temperature;

import java.util.HashMap;

public class Quoter {

    private HashMap<String, Double> list ;

    public Quoter(){
        list = new HashMap<>();
        list.put("1", 10.0);
        list.put("2", 20.0);
        list.put("3", 30.0);
        list.put("4", 40.0);
        list.put("5", 50.0);
    }

    public double getBookPrice(String isbn){
        Double result = list.get(isbn);
        if(result == null){
            return 0;
        }
        else return result;
    }
}
