package nl.utwente.di.temperature;

public class TemperatureConverter {

    public double convertCelsius(double celsius){
        return (double) (celsius * 9) /5 + 32;
    }

}
